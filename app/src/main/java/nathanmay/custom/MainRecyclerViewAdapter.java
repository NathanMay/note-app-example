package nathanmay.custom;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import nathanmay.database.Task;
import nathanmay.task3r.R;
import nathanmay.task3r.ViewTaskActivity;

/**
 * Created by Nathan on 17/12/2017.
 */

public class MainRecyclerViewAdapter extends RecyclerView.Adapter<MainRecyclerViewAdapter.taskViewHolder>  {
    /**
     * Change ArrayList to contain TASK objects.
     */
    public  ArrayList<Task> mTaskList = new ArrayList<>();
    public Context mContext;
    public static int mTouchedElementPriority;

    public class taskViewHolder extends RecyclerView.ViewHolder {
        //what is required within the UI

        public TextView taskTitle;
        public TextView taskDesc;
        public TextView taskDueDate;
        public View taskStatus;


        public taskViewHolder(View view){ //Constructor
            super(view);

            taskTitle = view.findViewById(R.id.taskTitle);
            taskDesc = view.findViewById(R.id.taskDesc);
            taskDueDate = view.findViewById(R.id.taskDueDate);
            taskStatus = view.findViewById(R.id.taskStatus);

        }
    }

    public MainRecyclerViewAdapter(Context context , ArrayList<Task> tasks){
        this.mContext = context;
        this.mTaskList = tasks;

    }

    @Override
    public taskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_cell, parent, false);
        return new taskViewHolder(v);
    }

    @Override
    public void onBindViewHolder(taskViewHolder holder, int position) {
        Task task = mTaskList.get(position); //Get the task for each index
        final int taskID = task.getmID();
        holder.taskTitle.setText(task.getmTitle());
        holder.taskDesc.setText(task.getmText());
        holder.taskDueDate.setText(task.getmDueDate());
        holder.taskStatus.setBackgroundColor(getPriorityColour(task.getmPriority()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Open the edit task activity
                Intent intent = new Intent(mContext, ViewTaskActivity.class);
                intent.putExtra("TASKID", taskID);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mTaskList.size();
    }

    public static int getPriorityColour(int taskPriority){
        /**
         * Switch statement to return colour depending on Priority
         * TODO change these to get from resources.
         */
        switch(taskPriority){
            case 0: return Color.parseColor("#000000");//Null. Black?
            case 1: return Color.parseColor("#81C784");//Low
            case 2: return Color.parseColor("#29B6F6");//Normal
            case 3: return Color.parseColor("#EF5350");//High
        }
        //Return default
        return Color.parseColor("#29B6F6");
    }

    public void updateTaskList( ArrayList<Task> tasks){
        mTaskList.clear();
        mTaskList.addAll(tasks);
        this.notifyDataSetChanged();
    }

}
