package nathanmay.custom;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;


/**
 * Created by Nathan on 04/01/2018.
 * Created with guidance from:
 * https://www.codeproject.com/Tips/524556/Android-customize-EditText-like-a-page-in-textbook
 *
 */

public class NotepadEditText extends android.support.v7.widget.AppCompatEditText {

    private Rect mRectangle;
    private Paint mPaint;
    private int mHeight;
    private int mLineHeight;


    public NotepadEditText(Context context, AttributeSet attributes){
        super(context, attributes);

        mRectangle = new Rect();
        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(2);
        mPaint.setColor(Color.parseColor("#29B6F6"));

    }

    @Override
    protected void onDraw(Canvas canvas) {
        mHeight = getHeight();
        mLineHeight = getLineHeight();
        int lineNumber = mHeight / mLineHeight;
        int numberOfLines = getLineCount();

        if(numberOfLines > lineNumber){
            lineNumber = getLineCount();
        }
        Rect rect = mRectangle;
        Paint paint = mPaint;

        int firstLine = getLineBounds(0, rect);

        for(int i = 0; i < lineNumber; i++){
            canvas.drawLine(rect.left, firstLine + 14 , rect.right, firstLine + 14, paint);
            firstLine = firstLine + getLineHeight();
        }
        super.onDraw(canvas);
    }
}
