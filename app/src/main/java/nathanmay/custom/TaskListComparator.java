package nathanmay.custom;

import java.util.Comparator;

import nathanmay.database.Task;

/**
 * Created by Nathan on 11/01/2018.
 */

public class TaskListComparator implements Comparator<Task> {

    @Override
    public int compare(Task task1, Task task2) {
        return Integer.compare(task1.getmPriority(), task2.getmPriority());

    }
}

