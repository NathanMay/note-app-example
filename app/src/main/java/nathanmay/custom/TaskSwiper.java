package nathanmay.custom;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import nathanmay.task3r.MainActivity;

import static android.support.v7.widget.helper.ItemTouchHelper.*;

/**
 * Created by Nathan on 06/01/2018.
 */

public class TaskSwiper extends Callback {

    public boolean mBackSwiped;
    public boolean mDrawHint;

    private RectF mRect;
    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        return makeMovementFlags(0, RIGHT);
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return false;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

    }

    @Override
    public int convertToAbsoluteDirection(int flags, int layoutDirection) {
        if(mBackSwiped){
            mBackSwiped = false;
            return 0;
        }
        return super.convertToAbsoluteDirection(flags, layoutDirection);
    }

    //Used to draw in the view
    @Override
    public void onChildDraw(Canvas canvas, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float xPosition, float yPosition, int actionState, boolean isCurrentlyActive) {
        //When swiping...

        if(actionState == ACTION_STATE_SWIPE){

            //Draws to the screen but clips the drawable area to the very left hand side of the sliding view.
            if(xPosition > 0){
                drawHint(canvas, viewHolder, (int) xPosition);
                Log.d("TEST: ", "On Screen at X pixel " + String.valueOf(xPosition));

            }
            if(xPosition >= MainActivity.mScreenWidth){
                Log.d("TEST: ", "Off Screen at X pixel " + String.valueOf(xPosition));

            }

        }
        super.onChildDraw(canvas, recyclerView, viewHolder, xPosition, yPosition, actionState, isCurrentlyActive);
    }

    private void drawHint(Canvas c, RecyclerView.ViewHolder v, int maximumRightDrawCoord) {
        View itemView = v.itemView;
        Paint p = new Paint();
        RectF points = new RectF(itemView.getLeft(), itemView.getTop(), itemView.getLeft() + (itemView.getRight() / 4), itemView.getBottom());
        p.setColor(MainRecyclerViewAdapter.getPriorityColour(MainRecyclerViewAdapter.mTouchedElementPriority));
        p.setTextSize(60);
        c.clipRect(itemView.getLeft(), itemView.getTop(), maximumRightDrawCoord, itemView.getBottom());
        c.drawText("Swipe Right to Complete", points.left + 20, points.bottom - 20,  p);

    }


}
