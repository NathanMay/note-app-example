package nathanmay.database;
import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by Nathan on 29/12/2017.
 */

@Entity(tableName = "tasks")
public class Task {

    @PrimaryKey(autoGenerate = true)
    public int mID;

    @ColumnInfo(name = "taskTitle")
    public String mTitle;

    @ColumnInfo(name = "taskText")
    public String mText;

    @ColumnInfo(name="taskDueDate")
    public String mDueDate;

    @ColumnInfo(name="taskPriority")
    public int mPriority;

    @ColumnInfo(name = "taskCompleted")
    public boolean mCompleted;

    public boolean ismCompleted() {
        return mCompleted;
    }

    public void setmCompleted(boolean mCompleted) {
        this.mCompleted = mCompleted;
    }

    public int getmPriority() {
        return mPriority;
    }

    public void setmPriority(int mPriority) {
        this.mPriority = mPriority;
    }

    public void incrementPriority(){
        this.mPriority++;
    }

    public String getmDueDate() {
        return mDueDate;
    }

    public void setmDueDate(String mDueDate) {
        this.mDueDate = mDueDate;
    }

    public int getmID() {
        return mID;
    }

    public String getmTitle() {
        return mTitle;
    }

    public String getmText() {
        return mText;
    }

    //Used when updating tasks.
   public void setmID(int mID) {
        this.mID = mID;
   }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public void setmText(String mText) {
        this.mText = mText;
    }
}
