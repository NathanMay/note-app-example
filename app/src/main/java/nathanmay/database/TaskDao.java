package nathanmay.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

/**
 * Created by Nathan on 29/12/2017.
 */
@Dao
public interface TaskDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long[] insertAll(Task... tasks);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateTask(Task t);

    @Delete
    void deleteTask(Task t);

    @Query("SELECT * FROM tasks WHERE mID LIKE  :id")
    Task getTaskFromId(int id);


    @Query("SELECT * FROM tasks ORDER BY taskPriority DESC")
    List<Task> getAllTasksDescendingPriority();

    @Query("SELECT * FROM tasks")
    List<Task> getAllTasks();

    @Query("DELETE FROM tasks")
    void destroyTasksTable();





}
