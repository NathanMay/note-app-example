package nathanmay.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Nathan on 29/12/2017.
 * An abstract class, extending RoomDatabase, that lists the entities and the DAOs which
 * access them.
 */
@Database(entities = {Task.class}, version = 4) //Added due date
public abstract class TaskDatabase extends RoomDatabase {

    private static TaskDatabase INSTANCE;
    public abstract TaskDao taskDao();

    public static TaskDatabase getTaskDatabase(Context context) {
        if( INSTANCE == null){
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), TaskDatabase.class,
                    "task-database").allowMainThreadQueries().addMigrations(Migration_3_to_4).build();


        }

        return INSTANCE;
    }

//    static final Migration Migration_1_to_2 = new Migration(1, 2) {
//        @Override
//        public void migrate(@NonNull SupportSQLiteDatabase database) {
//            database.execSQL("ALTER TABLE tasks ADD COLUMN taskDueDate TEXT");
//        }
//    };

//    static final Migration Migration_2_to_3 = new Migration(2, 3) {
//        @Override
//        public void migrate(@NonNull SupportSQLiteDatabase database) {
//            database.execSQL("ALTER TABLE tasks ADD COLUMN taskPriority INTEGER NOT NULL DEFAULT 0");
//        }
//    };

        static final Migration Migration_3_to_4 = new Migration(3, 4) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE tasks ADD COLUMN taskCompleted INTEGER NOT NULL DEFAULT 0");
        }
    };



    public static void destroyInstance(){
        INSTANCE = null;
    }

    public static void updateTask(TaskDatabase db, Task t){
       db.taskDao().updateTask(t);
       }

    public static void removeTask(TaskDatabase db, Task t){
        db.taskDao().deleteTask(t);
    }

    /** ADD BUTTON TO MAIN WHICH CLEANS ALL THE COMPLETED TASKS FROM THE DATABASE. **/
    public static void removeCompletedTasks(TaskDatabase db){
        List<Task> allTasks = db.taskDao().getAllTasks();
        for(int taskIndex = 0; taskIndex < allTasks.size(); taskIndex++){
            Task checkedTask = allTasks.get(taskIndex);
            if(checkedTask.ismCompleted()){
                removeTask(db, checkedTask);
            }

        }
        Log.d("TEST: ", "Cleared Complete Tasks.\n");
        printAllTasksAsText(db);


    }

    public static ArrayList<Task> getAllTasks(TaskDatabase db) {
        List<Task> allTasks = db.taskDao().getAllTasks();
        ArrayList<Task> taskContainer = new ArrayList<>();
        for (int i = 0; i < allTasks.size(); i++) {
            Task givenTask = allTasks.get(i);
            taskContainer.add(givenTask);
        }
        return taskContainer;
    }

    public static ArrayList<Task> getAllTasksDescendingPriority(TaskDatabase db){
        List<Task> allTasks = db.taskDao().getAllTasksDescendingPriority();
        ArrayList<Task> taskContainer = new ArrayList<>();
        for (int i = 0; i < allTasks.size(); i++) {
            Task givenTask = allTasks.get(i);
            taskContainer.add(givenTask);
        }
        return taskContainer;
    }

    public static Task getTaskFromId(TaskDatabase db, int id){
        Task taskViaId = db.taskDao().getTaskFromId(id);
        return taskViaId;
    }

    public static Task addTask(TaskDatabase db, Task task){
        db.taskDao().insertAll(task);
        return task;
    }

    public static void nukeTaskTable(TaskDatabase db){
        db.taskDao().destroyTasksTable();

    }

    public static void addTestData(TaskDatabase db){

        ArrayList<Task> testTasks = new ArrayList<>();
        Task t1 = new Task();
        t1.setmTitle("Go Grocerie Shopping");
        t1.setmText("Eggs, Bacon, Bread, Butter, Milk");
        t1.setmDueDate("Tuesday");
        testTasks.add(t1);

        Task t2 = new Task();
        t2.setmTitle("Fix Kitchen Sink");
        t2.setmText("Need: Glue, Duct Tape, Hammer");
        t2.setmDueDate("Wednesday");
        testTasks.add(t2);

        Task t3 = new Task();
        t3.setmTitle("Implement more features");
        t3.setmText("Adding notes via UI");
        t3.setmDueDate("Friday");
        testTasks.add(t3);

        Task t4 = new Task();
        t4.setmTitle("Check for new songs");
        t4.setmText("Spotify");
        t4.setmDueDate("Monday");
        testTasks.add(t4);

        Task t5 = new Task();
        t5.setmTitle("Test");
        t5.setmText("Test");
        t5.setmDueDate("Wednesday");
        testTasks.add(t5);

        Task t6 = new Task();
        t6.setmTitle("Test");
        t6.setmText("Test");
        t6.setmDueDate("ASAP");
        testTasks.add(t6);

        Task t7 = new Task();
        t7.setmTitle("Test");
        t7.setmText("Test");
        t7.setmDueDate("Monday");
        testTasks.add(t7);

        Task t8 = new Task();
        t8.setmTitle("Test");
        t8.setmText("Test");
        t8.setmDueDate("Wednesday");
        testTasks.add(t8);

        Task t9 = new Task();
        t9.setmTitle("Test");
        t9.setmText("Test");
        t9.setmDueDate("ASAP");
        testTasks.add(t9);

        for(int i = 0 ; i < testTasks.size(); i++){
            Task currentTask = testTasks.get(i);
            if(currentTask.getmPriority() == 0){ //If priority is not set
                int randomPriority = ThreadLocalRandom.current().nextInt(0, 4);
                currentTask.setmPriority(randomPriority);
                addTask(db, currentTask);
            }
        }
    }

    public static void printAllTasksAsText(TaskDatabase db){
        List<Task> list = db.taskDao().getAllTasks();
        Log.d("TEST: " , "=====================================");
        for(int i = 0; i < list.size(); i++){
            Task task = list.get(i);
            Log.d("TEST: ",  String.valueOf(task.getmID()) +  " | " + task.getmTitle()
                    + " | " + task.getmText() + " | " +  task.getmDueDate() + " | " + task.getmPriority() + " | " + task.ismCompleted());
        }
        Log.d("TEST: " , "=====================================");
    }





}
