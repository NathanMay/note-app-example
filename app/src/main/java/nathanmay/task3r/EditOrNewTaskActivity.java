package nathanmay.task3r;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import nathanmay.database.Task;
import nathanmay.database.TaskDatabase;

/**
 * Created by Nathan on 04/01/2018.
 */

public class EditOrNewTaskActivity extends AppCompatActivity {

    private int mTaskID;
    private TaskDatabase mTaskDB;
    private Task mCurrentTask;
    private TextView mTitleEdit;
    private TextView mTextEdit;
    private TextView mDueDateEdit;
    private View mPriorityBar;
    private ImageView mPriorityImage;
    private CardView mCardView;
    private boolean mIsNewTask;
    private boolean mTaskCreationSuccessful;
    private int[] mColorList = new int[4];
    private int mIdHandle;
    private DatePickerDialog.OnDateSetListener mChosenDate;
    private Calendar mTaskDatePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_task_cards);

        mTaskDB = TaskDatabase.getTaskDatabase(getApplicationContext());
        Intent intent = getIntent();
        mTaskID = intent.getExtras().getInt("TASKID");
        String taskMode = intent.getStringExtra("MODE");
        mColorList[0] = getResources().getColor(R.color.black);
        mColorList[1] = getResources().getColor(R.color.lowPriority);
        mColorList[2] = getResources().getColor(R.color.normalPriority);
        mColorList[3] = getResources().getColor(R.color.highPriority);

        if(taskMode != null && taskMode.equals("NewTask")){
            mIsNewTask = true;
            setTitle("New Task");
            mCurrentTask = getBlankTask();
            initUI();
        }
        else{
            mIsNewTask = false;
            setTitle("Edit Task");
            mCurrentTask =  mTaskDB.taskDao().getTaskFromId(mTaskID);
            initUI();
        }

        setClickListeners();

    }
    private Task getBlankTask() {
        Task blankTask = new Task();
        blankTask.setmTitle("");
        blankTask.setmText("");
        blankTask.setmDueDate("");
        blankTask.setmPriority(0);
        return blankTask;
    }
    private void initUI(){
        /** Set UI Elements **/
        mCardView = findViewById(R.id.card_view);
        mTitleEdit = findViewById(R.id.taskTitleEdit);
        mTextEdit = findViewById(R.id.taskTextEdit);
        mDueDateEdit = findViewById(R.id.taskDueDateEdit);
        mPriorityBar = findViewById(R.id.taskStatusBar);
        mPriorityImage = findViewById(R.id.taskPriorityImage);
        mTaskDatePicker = Calendar.getInstance();

        mTitleEdit.setText(mCurrentTask.getmTitle());
        mTextEdit.setText(mCurrentTask.getmText());
        if(mCurrentTask.getmDueDate().equals("")){
            mDueDateEdit.setText("Date Not Set");
        }else{
            mDueDateEdit.setText(mCurrentTask.getmDueDate());
        }
        if(mCurrentTask.getmPriority() == 0){
            mPriorityImage.setImageResource(R.drawable.nopriority);
            mPriorityBar.setBackgroundColor(mColorList[0]);
        }
        if(mCurrentTask.getmPriority() == 1){
            mPriorityImage.setImageResource(R.drawable.lowpriority);
            mPriorityImage.setColorFilter(mColorList[1], PorterDuff.Mode.SRC_ATOP);
            mPriorityBar.setBackgroundColor(mColorList[1]);
        }
        if(mCurrentTask.getmPriority()  == 2){
            mPriorityImage.setImageResource(R.drawable.normalpriority);
            mPriorityImage.setColorFilter(mColorList[2], PorterDuff.Mode.SRC_ATOP);
            mPriorityBar.setBackgroundColor(mColorList[2]);
        }
        if(mCurrentTask.getmPriority()  == 3){
            mPriorityImage.setImageResource(R.drawable.highpriority);
            mPriorityImage.setColorFilter(mColorList[3], PorterDuff.Mode.SRC_ATOP);
            mPriorityBar.setBackgroundColor(mColorList[3]);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_task, menu);
        return super.onCreateOptionsMenu(menu);
    }
    private void assembleCreatedTask() {
        if(mIsNewTask){
            Task newTask = new Task();
            newTask.setmTitle(mTitleEdit.getText().toString());
            newTask.setmText(mTextEdit.getText().toString());
            /**CHANGE THIS TO GET THE NEW PRIORITY **/
            newTask.setmPriority(mCurrentTask.getmPriority());
            newTask.setmDueDate(mDueDateEdit.getText().toString());

            /** Add the new task to the database, which automatically sets its ID**/
            updateOrInsertTask(mIsNewTask, newTask);
        }else{
            Task updatedTask = new Task();
            updatedTask.setmID(mCurrentTask.getmID());
            updatedTask.setmTitle(mTitleEdit.getText().toString());
            updatedTask.setmText(mTextEdit.getText().toString());
            /**CHANGE THIS TO GET THE NEW PRIORITY **/
            updatedTask.setmPriority(mCurrentTask.getmPriority());
            updatedTask.setmDueDate(mDueDateEdit.getText().toString());
            updateOrInsertTask(mIsNewTask, updatedTask);
        }

    }
    private void updateOrInsertTask(boolean isNewTask, Task createdTask) {
        /** If the task is new, and input fields are OK... **/
        if(isNewTask && inputFieldsAreValid(createdTask)){
            //Here, we insert the passed task into the DB, but also return its ID.
            long[] newTaskID = mTaskDB.taskDao().insertAll(createdTask);
            mIdHandle = (int) newTaskID[0];
            mTaskCreationSuccessful = true;
        }
        /** If the task is not new, and is just an edit, and input fields are OK... **/
        else if(!isNewTask && inputFieldsAreValid(createdTask)){
            mIdHandle = createdTask.getmID();
            mTaskDB.taskDao().updateTask(createdTask);
            mTaskCreationSuccessful = true;
        }else{
            mTaskCreationSuccessful = false;
            Log.d("TEST" , "INVALID TASK");
        }


    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int clickId = item.getItemId();
        switch (clickId) {
            case R.id.action_saveChanges:
                assembleCreatedTask();
                Intent viewTask = new Intent(EditOrNewTaskActivity.this, ViewTaskActivity.class);
                if(mIsNewTask && mTaskCreationSuccessful){
                    viewTask.putExtra("TASKID", mIdHandle);
                    startActivity(viewTask);
                    break;
                }
                if(!mIsNewTask && mTaskCreationSuccessful){
                    viewTask.putExtra("TASKID", mIdHandle);
                    startActivity(viewTask);
                    break;
                }

        }
        return super.onOptionsItemSelected(item);
    }
    private void setClickListeners() {
        mChosenDate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                mTaskDatePicker.set(Calendar.YEAR, year);
                mTaskDatePicker.set(Calendar.MONTH, month);
                mTaskDatePicker.set(Calendar.DAY_OF_MONTH, day);
                String format = "dd/MM/yy";
                SimpleDateFormat formattedDate = new SimpleDateFormat(format, Locale.UK);
                String dueDate = formattedDate.format(mTaskDatePicker.getTime());
                mDueDateEdit.setText(dueDate);
            }
        };

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.taskDueDateEdit:
                        new DatePickerDialog(EditOrNewTaskActivity.this, mChosenDate, mTaskDatePicker.get(Calendar.YEAR),
                                mTaskDatePicker.get(Calendar.MONTH), mTaskDatePicker.get(Calendar.DAY_OF_MONTH)).show();
                        break;
                }
            }


        };
        mDueDateEdit.setOnClickListener(listener);
    }
    public void cyclePriorities(View priorityButton){
        int currentPriority = mCurrentTask.getmPriority();

        int[] drawableList = {R.drawable.nopriority, R.drawable.lowpriority, R.drawable.normalpriority, R.drawable.highpriority};
        //Therefore, the current colour is colorlist[currentpriority];
        if(currentPriority == 3){ //We will be changing the priority and want to roll back to 0.
            mCurrentTask.setmPriority(0);
            mPriorityImage.setImageResource(drawableList[0]);
            mPriorityImage.setColorFilter(mColorList[0], PorterDuff.Mode.SRC_ATOP);
            mPriorityBar.setBackgroundColor(mColorList[0]);
        }else{
            mCurrentTask.incrementPriority();
            mPriorityImage.setImageResource(drawableList[currentPriority + 1]);
            mPriorityImage.setColorFilter(mColorList[currentPriority + 1], PorterDuff.Mode.SRC_ATOP);
            mPriorityBar.setBackgroundColor(mColorList[currentPriority + 1]);
        }
    }
    private boolean inputFieldsAreValid(Task inputTask){
        if (inputTask.mTitle.toString().matches("[A-Za-z0-9 - ]+") && !inputTask.mTitle.equals("") && inputTask.mTitle.length() > 1 ) {
            return true;
        }else {
            Toast notify = Toast.makeText(this, "Task Title must contain text", Toast.LENGTH_LONG);
            notify.show();
            return false;
        }
    }
}
