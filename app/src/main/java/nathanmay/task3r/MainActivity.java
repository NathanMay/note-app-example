package nathanmay.task3r;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import nathanmay.custom.MainRecyclerViewAdapter;
import nathanmay.database.Task;
import nathanmay.database.TaskDatabase;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private MainRecyclerViewAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<Task> mTaskList;
    private ArrayList<Task> mIncompletedList;
    private ArrayList<Task> mCompletedList;
    private TaskDatabase mTaskDB;
    private ImageView mEmptyListIcon;
    private TextView mEmptyListText;
    public static int mScreenWidth;
    public static int mScreenHeight;
    private boolean mSortPriorityDesc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initDB();
        initUI();
    }

    //Create action bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int clickId = item.getItemId();

        switch(clickId){
            case R.id.action_create:
                Intent intent = new Intent(this, EditOrNewTaskActivity.class);
                intent.putExtra("MODE", "NewTask");
                this.startActivity(intent);
                break;
            case R.id.action_clear_complete:
                mTaskDB.removeCompletedTasks(mTaskDB);
                Toast notify = Toast.makeText(this, "Cleared Completed Tasks", Toast.LENGTH_LONG);
                notify.show();
            case R.id.action_sort_priority:
                /**
                 * Needs Shared Preferences as it is a setting
                 */
                mSortPriorityDesc = !mSortPriorityDesc; //Invert priority sorting
                mTaskList = TaskDatabase.getAllTasksDescendingPriority(mTaskDB);
                mAdapter.updateTaskList(mTaskList);
        }
        return super.onOptionsItemSelected(item);
    }



    private void initDB() {
        mTaskDB = TaskDatabase.getTaskDatabase(getApplicationContext());
        /**
         * TODO pass these actions to an asynchronous task.
         * TODO creating ArrayLists from DB may be costly with huge lists. Can this be loaded passively?
         * TODO Passive loading may require magic.
         */

        mTaskList = TaskDatabase.getAllTasks(mTaskDB);

        mIncompletedList = new ArrayList<>();
        mCompletedList = new ArrayList<>();
        //Over all tasks, find those complete and incomplete and add them to their respective holders.
        for(int i = 0; i < mTaskList.size(); i++){
            Task checkedTask = mTaskList.get(i);
            if(!checkedTask.ismCompleted()){  //If the task is NOT complete...
                mIncompletedList.add(checkedTask);
            } //Could use an else but this could hurt in the future.
            if(checkedTask.ismCompleted()){
                mCompletedList.add(checkedTask);

            }
        }
    }

    private void initUI() {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        mScreenHeight = metrics.heightPixels;
        mScreenWidth = metrics.widthPixels;
        mRecyclerView = findViewById(R.id.mainRecyclerView);

        ItemTouchHelper.SimpleCallback swipe = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

                int swipePosition = viewHolder.getAdapterPosition();
                Task swipedTask = mIncompletedList.get(swipePosition);

                if(direction == ItemTouchHelper.RIGHT) {
                    completeTask(swipedTask, swipePosition);

                }
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(swipe);
        itemTouchHelper.attachToRecyclerView(mRecyclerView); //set swipe to recylcerview

        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new MainRecyclerViewAdapter(MainActivity.this, mIncompletedList);


        Drawable divider = ContextCompat.getDrawable(this, R.drawable.recycler_view_divider);

        mRecyclerView.addItemDecoration(new RecyclerViewDecoration(divider));
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        mEmptyListIcon = findViewById(R.id.emptyListIcon);
        mEmptyListText = findViewById(R.id.emptyListText);
        if(mTaskList.size() == 0){
            Log.d("TEST: ", "Task list is empty!");
            mEmptyListIcon.setVisibility(View.VISIBLE);
            mEmptyListText.setVisibility(View.VISIBLE);
        }else{
            mEmptyListIcon.setVisibility(View.GONE);
            mEmptyListText.setVisibility(View.GONE);
        }



    }

    private void completeTask(final Task swipedTask, final int swipePosition) {
        swipedTask.setmCompleted(true); //set incoming task to true
        mTaskDB.taskDao().updateTask(swipedTask);
        mIncompletedList.remove(swipedTask);
        mAdapter.notifyItemRemoved(swipePosition);
        Log.d("TEST: ", "YOU COMPLETED " + swipedTask.getmTitle());
        Snackbar undoSnackBar = Snackbar.make(mRecyclerView, "Completed Task!", Snackbar.LENGTH_LONG)
                .setAction("UNDO", new View.OnClickListener() {

                            @Override
                            public void onClick(View view) {
                                /**
                                 * Here, I create a copy of the incoming task.
                                 * Then, its state is set to "complete", and its added to the database.
                                 * After this, it is also added to the Incompleted Tasks list.
                                 * I then notify the adapter of a state change, and print the tasks for debugging purposes.
                                 */
                                Task copiedTask = swipedTask;
                                copiedTask.setmCompleted(false);
                                mTaskDB.taskDao().insertAll(copiedTask);
                                mIncompletedList.add(copiedTask);
                                mAdapter.notifyItemChanged(swipePosition);
                                mTaskDB.printAllTasksAsText(mTaskDB);
                            }
                        });
        undoSnackBar.show();
    }

}
