package nathanmay.task3r;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Nathan on 29/12/2017.
 */

public class RecyclerViewDecoration extends RecyclerView.ItemDecoration {

    private Drawable mDivider;

    public RecyclerViewDecoration(Drawable drawable){
        mDivider = drawable;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        if(parent.getChildAdapterPosition(view) == 0){
            return;
        }

        outRect.top = mDivider.getIntrinsicHeight();
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {

        int left = parent.getPaddingLeft();
        int right = parent.getWidth() - parent.getPaddingRight();
        int children = parent.getChildCount();

        for(int i = 0; i < children ; i ++){
            View child = parent.getChildAt(i);
            RecyclerView.LayoutParams parameters = (RecyclerView.LayoutParams) child.getLayoutParams();
            int top = child.getBottom() + parameters.bottomMargin;
            int bottom = top + mDivider.getIntrinsicHeight();
            mDivider.setBounds(left, top, right, bottom);
            mDivider.draw(c);
        }

    }
}
