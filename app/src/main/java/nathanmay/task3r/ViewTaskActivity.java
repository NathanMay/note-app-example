package nathanmay.task3r;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import nathanmay.custom.MainRecyclerViewAdapter;
import nathanmay.database.Task;
import nathanmay.database.TaskDatabase;

/**
 * Created by Nathan on 03/01/2018.
 */

public class ViewTaskActivity extends AppCompatActivity {

    private int mTaskID;
    private TaskDatabase mTaskDB;
    private Task mCurrentTask;
    private TextView mTitleText;
    private TextView mTextText;
    private TextView mDueDateText;
    private ImageView mPriorityImage;
    private View mStatusView;
    private CardView mCardView;

    public int mNoPriorityColor;
    public int mLowPriorityColor;
    public int mNormalPriorityColor;
    public int mHighPriorityColor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_task_cards);
        setTitle("");

        mTaskDB = TaskDatabase.getTaskDatabase(getApplicationContext());

        mCurrentTask = getTaskFromIntent();

        mNoPriorityColor = getResources().getColor(R.color.black);
        mLowPriorityColor = getResources().getColor(R.color.lowPriority);
        mNormalPriorityColor = getResources().getColor(R.color.normalPriority);
        mHighPriorityColor = getResources().getColor(R.color.highPriority);

        mCardView = findViewById(R.id.card_view);
        mTitleText = findViewById(R.id.taskTitleView);
        mTextText = findViewById(R.id.taskTextView);
        mDueDateText = findViewById(R.id.taskDueDateView);
        mStatusView = findViewById(R.id.taskStatusView);
        mPriorityImage = findViewById(R.id.taskPriorityImage);

        mTitleText.setText(mCurrentTask.getmTitle());
        mTextText.setText(mCurrentTask.getmText());
        mDueDateText.setText(mCurrentTask.getmDueDate());
        mStatusView.setBackgroundColor(MainRecyclerViewAdapter.getPriorityColour(mCurrentTask.getmPriority()));

        if(mCurrentTask.getmPriority() == 0){
            mPriorityImage.setImageResource(R.drawable.nopriority);

        }
        if(mCurrentTask.getmPriority() == 1){
            mPriorityImage.setImageResource(R.drawable.lowpriority);
            mPriorityImage.setColorFilter(Color.parseColor("#81C784"), PorterDuff.Mode.SRC_ATOP);
        }
        if(mCurrentTask.getmPriority()  == 2){
            mPriorityImage.setImageResource(R.drawable.normalpriority);
            mPriorityImage.setColorFilter(Color.parseColor("#29B6F6"), PorterDuff.Mode.SRC_ATOP);
        }
        if(mCurrentTask.getmPriority()  == 3){
            mPriorityImage.setImageResource(R.drawable.highpriority);
            mPriorityImage.setColorFilter(Color.parseColor("#EF5350"), PorterDuff.Mode.SRC_ATOP);
        }
    }

    //Create action bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_view_task, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int clickId = item.getItemId();
        switch(clickId){
            case R.id.action_edit:
                Intent editTask = new Intent(ViewTaskActivity.this, EditOrNewTaskActivity.class);
                editTask.putExtra("TASKID", mCurrentTask.getmID());
                startActivity(editTask);
                break;
            case R.id.action_delete:
                showConfirmationDialog("Delete", "Are you sure you'd like to delete this task?");
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showConfirmationDialog(String title, String question) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(
                new ContextThemeWrapper(ViewTaskActivity.this, R.style.confirmationDialog));
        dialogBuilder.setTitle(title);
        dialogBuilder.setMessage(question);
        dialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                TaskDatabase.removeTask(mTaskDB, mCurrentTask);
                dialogInterface.dismiss();
                Intent intent = new Intent(ViewTaskActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        dialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog confirmation = dialogBuilder.create();
        confirmation.show();
    }


    public Task getTaskFromIntent() {
        Intent intent = getIntent();
        mTaskID = intent.getExtras().getInt("TASKID");
        mTaskDB = TaskDatabase.getTaskDatabase(getApplicationContext());
        return mTaskDB.taskDao().getTaskFromId(mTaskID);
    }
}
